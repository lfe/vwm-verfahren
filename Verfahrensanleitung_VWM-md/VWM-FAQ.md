# FAQ und Hinweise - VWM 

## FIM 

### Wie kann ich Punkte offline bearbeiten, was gibt es zu beachten? 

Punkte können ohne Probleme offline bearbeitet werden. Nach dem Synchronisieren der Punkte (mit Internetverbindung) kann das QGis Projekt abgespeichert werden und die FIM Layer Permanent gemacht werden (Rechtsklick auf die FIM Layer > Permanent machen). Danach können die Punkte auch offline bearbeitet werden. Die Synchronisation über "LFB Anmelden" funktioniert allerdings nur mit **stabiler** Internetverbindung. 

### Ich kann die Unterlosnummer nicht ändern./ Ein Kollege hat Änderungen vorgenommen, die mir nicht angzeigt werden. 
Unterlosnummern sollten nur bei bestehender, stabiler Internetverbindung geändert werden. 
Am Besten teilt man einmalig, vor Beginn der Aufnahmen Unterlose ein und synchronisiert danach alle Geräte, die das Hauptlos bearbeiten. 

Wir übernehmen keine Haftung über die Unterlosfunktion, sie ist lediglich eine Handreichung und sollte gewissenhaft genutzt werden. 


### Ich habe kein Netz, was gibt es zu beachten? 

Das Bearbeiten der Punkte ohne Internetverbindung ist kein Problem, es sollte darauf geachtet werden, dass die FIM Layer permanent gemacht wurde und das QGIS Projekt nach jeder Aufnahmen gespeichert wird.

Zusätzlich kann nach jeder Aufnahme über "export .FIM" ein Backup der FIM Layer erstellt werden.

### Meine abgeschlossenen Punkte synchronisieren nicht, was soll ich tun? 

Möglicherweise ist der Punkt bereits durch jemand anderen bearbeitet worden. Oder es besteht keine stabile Internetverbindung. Wenn es mit stabiler Verbindung immernoch nicht klappt, sollte die Punkt ID notiert werden und ein Backup über "export .FIM" erstellt werden. Beides dann per Mail an landeswaldinventuren@lfb.brandenburg.de mit Angaben zum Problem.

### Der "Fertig" Button bleibt ausgegraut - wieso? 

Möglicherweise greift eine Plausibilitätsprüfung. Sind alle roten Ausrufezeichen weg? Falls ja, nochmal alle Reiter durchklicken und nachprüfen.

Punkt ID notieren und über "export .FIM" ein Backup der FIM Layer erstellen. E-Mail and landeswaldinventur@lfb.brandenburg.de mit Backupdatei und Punkt ID. 

### Ich habe einen Bug gefunden/ Etwas funktioniert nicht bei der Dateneingabe, was soll ich tun? 

E-Mail an landeswaldinventur@lfb.brandenburg.de mit **präziser** Angabe zum Bug. Möglichst genau und **reproduzierbar** beschreiben, wie es zum Fehler kommt (alle Umstände mit angeben!). Punkt ID angeben und über "export .FIM" ein Backup erstellen und an die Mail hängen. 

## Punkteinmessung

### Wiederholungsaufnahmen: Ich finde den vorherigen Baumplot nicht wieder, was soll ich tun? 

Wenn keiner der aufgenommenen Bäume auch nach **ausgiebiger Suche** nicht wiederzufinden sind, kann der Baumplot erneut eingemessen werden. Punkt und Transektrichtung sind in diesem Fall nicht zu verschieben. Es erfolgt lediglich eine Neuaufnahme der 5 nächsten Bäume vom Stichprobenpunkt. 


## Baumplot

### Wiederholungsaufnahmen: Was passiert mit  nicht gefundenen oder toten Bäumen im Baumplot? 

Diese sollten, wenn möglich und eindeutig zuzuordnen, aus der Eingabemasek entfernt werden (Löschen). Andernfalls können Sie auch einfach ignoriert werden. Sollten nun keine 5 Bäume mehr im Baumplot vorliegen, ist (falls möglich) ein weitere Baum als Ersatz aufzunehmen. 

### Was mach ich, wenn keine 5 Bäume im Umkreis von 12 m zu finden sind? 

Wenn keine 5 Bäume im Umkreis von 12 m zu finden sind, dann sind weiter entfernt stehende Bäume als Landmarken zu erfassen. Weiterhin sollten auch zusätliche Landmarken erfasst werden, falls diese vorhanden sind. 

## Verjüngungstransekt 

### Das Transekt (oder der Punkt) liegt zu teilen in einem gezäunten Bereich (Verjüngungsschutz), soll ich es verlegen? 

Nein, sofern ein betreten der gezäunten Fläche möglich ist, ohne den Zaun zu beschädigen, sollen die Aufnahmen auch innerhalb des Zauns erfolgen. Es ist stehts darauf zu achten Zäune nicht zu beschädigen und geschlossen zu halten. 

### Was mach ich, wenn eine Rückegasse das Transekt kreuzt? 

Solange die Gasse das Transekt nur kreuzt, ist dies nicht weiter relevant. Sollte mehr als die Hälfte des Transekt auf einer Rückegasse oder permanent unbestockten Fläche liegen, ist dieses zu verschieben. 

### Was mach ich, wenn Kronenreste oder anderes Totholz im Transekt liegen 

Dies ist als Temporäre Störung in der FIM Eingabemaske zu erfassen. Das Transekt muss nicht verlegt werden. 

### Wiederholungsaufnahmen: Das vorherige Transekt wurde verlegt, aber der Grund ist nicht ersichtlich. Soll ich das Transekt wieder auf 0 Gon legen?  

Nein, der Azimut des vorherigen Transekts ist beizubehalten, auch wenn kein Verlegegrund ersichtlich ist. Eine Verlegung erfolgt nur, wenn ein in der Aufnahmeanweisung genannter Grund für Verlegung am SP vorliegt 

## Weiserpflanzen/ Indikatorpflanzen 

### Was ist der Unterschied zwischen Weiserpflanzen und Indikatorpflanzen 

Beide Wörter meinen das Selbe. 

### Was ist mit "Bodenbedeckung mit Weiserpflanzen im Transekt" gemeint?  

Damit ist der Bodenbedeckungsgrad der genannten Weiserpflanzen im Transekt gemeint. Prozentualer Bedeckungsanteil der Weiserpflanze an der gesamten Transektfläche.

## Bestandesbeschreibung 
### Was ist der Unterschied zw reihenweise und streifenweise?

Streifenweise bezeichnet einen Streifen aus mehreren nebeneinander stehenden Bäumen. Eine Reiher besteht aus einzelnen Bäumen hintereinander. Ein Streifen ist also breiter als eine Reihe. 

### Was ist der Unterschied zw Naturverjüngung und Sukzession?

Sukzession findet auf vorher gestörten Flächen statt, zb Neubesiedlung einer Waldbrandfläche. Naturverjüngung ist also auch Teil der Sukzession, aber diese beschreibt eben eine Verjüngung nach Störung „Neubesiedlung“

### Ich kriege die Plausibilitätsprüfung bei Bestockung nicht weg, was mach ich falsch? 

Das FIM lässt nur bestimmte Kombinationen aus Schichtigkeit und Schichten zu. Zuerst sollte die angegebene Schichtigkeit geprüft werden und dann, ob die erfassten einzelnen Schichten, plausibel zur Schichtigkeit passen. 

Bei den regulären Aufnahmen sind die Schichten "zweischichtig mit Vorausverjüngung" und "zweischichtig mit Unterbau" nicht bzw. kaum relevant. Hier ist einfach "zweischichtig" auszuwählen. 