# Installation und Nutzung des QGis Plugins "FIM"

## Installation 
Das QGis Plugin FIM kann regulär über den Erweiterungsmanager von QGis installier werden. Zusammen mit FIM wird auch die Erweiterung GNAVS installiert, diese wird für die Verarbeitung der GNSS Daten benötigt. 

1. QGis > Erweiterungen > Erweiterungen verwalten und installieren 
2. Alle > "FIM" im Suchfeld eingeben > Erweiterung installieren 
3. Die Erweiterung GNAVS wird automatisch mit installiert, dies bitte bestätigen 

Nun sollte das FIM Icon in der QGis Oberfläche angezeigt werden. 

Mit Klick auf das Icon öffnet sich der FIM Layer

### importiere ausgwählte Punkte - zur Zeit nicht verfügbar
- importiert alle in QGIS ausgewählten Punkte in die FIM Layer und erstellt einen Eintrag pro Punkt
- ermöglicht somit die Aufnahme von selbst erstellten Punkten 

### export .FIM 
- gegenwärtige FIM Layer als .geojson exportieren 
- zur Sicherung aufgenommener Daten 
  
  Merke: bei Export wird immer die gesamte FIM Layer exportiert, sprich auch die nicht abgeschlossenen Punkte 

### import .FIM 
- importieren von exportieren FIM Layers im Format .geojson in die geöffnete FIM Layer 



### LFB Anmelden 
- öffnet ein Fenster zur Eingabe von Login Daten
- Bei erfolgreichem Login werden:
    - alle für den USER bereitgestellen Punkte in die FIM Layer geladen
    - Bereits ins FIM geladene, nicht abgeschlossene Punkte NICHT aktualisiert
    - alle abgeschlossenen Punkte an die Datenbank übermittelt und anschließend als "hochgeladen" gekennzeichnet

## Aufnahmeablauf mit Synchronisationsfunktion
- zunächst über LFB Sync, alle abzuarbeitenden Punkte in den FIM Layer laden
- das QGIs Projekt speichern und den FIM Layer permanent machen 
- Aufnahmen machen, zwischendurch das Projekt speichern um Daten zu sichern 
- Am Ende des Tages über "export .FIM" ein Backup der Daten erstellen und sicher abspeichern. Dann über "LFB Sync" die abgeschlossenen Punkte an die DB übertragen
- anschließend Projekt wieder speichern und am nächsten Tag mit Aufnahmen fortfahren 

### Unterlosnummer eintragen
- runtergeladene Punkte, können in Unterlose aufgeteilt werden
  - dazu die Punkte eines Unterloses entweder im FIM Layer oder auf der Karte auswählen
  - auf Schaltfläche "Unterlosnummer an X Trakten ändern" klicken und Unterlosnummer vergeben 
  - dies wird automatisch mit der DB synchronisiert, bitte NUR MIT BESTEHENDER INTERNETVERBINDUNG vornehmen 
  - danach sollten alle Bearbeiter des Loses, erneut Synchronisieren 

### Daten eintragen
- Punkt auswählen und auf "Bearbeiten" klicken
- Reiter einen nach dem anderen abarbeiten
- immer auf die Einheiten achten 
- nach Datenerhebung nochmals prüfen 
- am Ende der Aufnahme auf "FERTIG" klicken

### logische Prüfungen
- es gibt einige eingebaute logische Prüfungen im FIM (rotes Ausrufezeichen)
- Allgemein
  - Aufnahmetruppkürzel und GNSS-Gerätkürzel müssen vergeben werden, bitte EINHEITLICH und KONSISTENT 
- Koordinaten
  - die Punktkoordinaten müssen eingetragen werden 
- Baumplot 
  - die Transektrichtung muss eingetragen werden 
- Bestockung 
  - Anzahl der eingetragenen Schichten, muss der angegebenen Schichtigkeit des Bestandesbeschreibung entsprechen 
  - alle Schichtanteile, einer Schicht, müssen zusammen 100 % ergeben 
- Verjüngungstransekt 
  - wenn das Transekt kürzer als 200 dm ist, müssen genau 50 Pflanzen erfasst werden und die neue Transektlänge eingetragen werden
  

## Aufnahmen unabhängig von der VWM 
 - zur Zeit nicht möglich



