# Verfahrensanleitung zur Verjüngungszustands-und Wildeinflussmonitoring (VWM)

Aufnahmenanweisung

| Datum      | Version | Beschreibung           | Bearbeitungen      | Bearbeiter       |
| ---------- | ------- | ---------------------- | ------------------ | ---------------- |
| 07.02.2022 | 1       | Erste Produktivversion |                    | LFB-Abt.4-FB42   |
| 18.01.2023 | 1.1     | Überarbeitung          | 2 qm Verkleinerung | FB42             |
|            |         |                        | Dokumentation      | Projektant: EFCA |
| 12.01.2024 | 1.2     | Überarbeitung          | redaktionelles     | FB42             |

## Präambel

Die Inventur „Verjüngungszustands- und Wildeinflussmonitoring (VWM)“ ist eine Waldinventur nach
§ 30 LWaldG die landesweit in allen Eigentumsformen durchgeführt wird. Sie ist ein Instrument der Politikberatung und hat das Ziel, Aussagen zur Verjüngung (nachwachsende Waldgeneration) sowie dem Zustand dieser Verjüngung zu finden und darzustellen. Folgende Daten werden erfasst:

- das Vorhandensein von Verjüngung,
- die Art der Verjüngung,
- der Zustand der Verjüngung,
- Angaben zum Waldbestand.

Mit diesen Daten sollen Informationen zur

- Verjüngungsart,
- Verjüngungshöhe,
- Verjüngungsentwicklung,
- Verjüngungsdichte,
- Wildverbiss,
- Schäle und
- biotische sowie abiotische Entwicklungseinschränkungen

generiert werden.

Die Informationen werden zukünftig für alle öffentlich zugänglich im Internet in einem
Informationsdashboard abrufbar sein.

## Datenhaltung und Aufnahmesoftware

Die Datenhaltung der Inventurdaten erfolgt zentral auf einem Server des Landesbetriebes Forst
Brandenburg - Abt. 4 Landeskompetenzzentrum Forst Eberswalde. Die Inventurdaten beinhalten
keine personenbezogenen oder personenbeziehbaren Daten.

Sämtliche für das VWM erhobenen Daten werden digital in mobilen Datenerfassungsgeräten in
QGIS erfasst. Für die Inventur werden Dateien mit der nötigen Datenstruktur und den
Stichprobenpunkten (SPs) an denen jeweils Aufnahmen erfolgen, zum Download bereitgestellt.

In Wiederholungsaufnahmen gehört der Baum- und Landmarkenplot zu den übermittelten Grunddaten.

## Aufnahmeanweisung

### Stichprobenpunkt - Aufnahmepunkt

Alle Aufnahmen zur VWM erfolgen am oder vom Stichprobenpunkt (**SP**). Die Stichprobenpunkte
werden vom Auftraggeber vorgegeben und digital als Geoobjekt übergeben. Der jeweilige
Stichprobenpunkt ist mit GNSS-Unterstützung aufzusuchen und temporär mit einer Fluchtstange zu
markieren. Zusätzlich zur temporären Markierung mit der Fluchtstange ist, *wenn* das Bestandesbild dies zum
Wiederfinden erfordert z.B. in dichtem Kiefernstangenholz, mit blauer
Forstmarkierfarbe der nächststehende Baum am Stammfuß mit einem maximal 5 cm großen Kreis in
ca. 50 cm Höhe vom Boden aus in Richtung des SP, zu markieren. Von diesem Baum aus beginnt in
Uhrzeigerrichtung die Aufnahme – Baumplot 1 (**BP 1**). Der Aufnahmepunkt (**AP**) ist der Startpunkt des
Verjüngungstransektes (**VT**), dieser beginnt in 2 m Entfernung vom SP in Transektrichtung. Am Ende des Transektes ist Baumplot 2 (**BP 2**) aufzunehmen.

Hieraus ergibt sich folgender Abarbeitungsprozedur:
SP &rarr; BP 1 &rarr; AP &rarr; VT-Richtung &rarr; BP 2 &rarr; VT &rarr; Bestandesbeschreibung am SP.

#### Verlegung des Stichprobenpunktes - Aufnahmepunktes

Der Punkt ist um maximal 50 m zu verlegen, wenn:

1. er auf einer Straße (Land-, Bundes-, Kreisstraße) liegt;
2. er näher als 15 m an einer öffentlichen Straße liegt;
3. er auf einem Haupt- oder Zubringerweg liegt;
4. er auf einer Trasse oder anderen dauerhaft unbestockten Fläche liegt;
5. er in einem dauerhaft abgesperrten Areal (z.B. gezäuntes Gelände) liegt.

Vom Auszuführenden ist dann ein geeigneter Ersatzpunkt zu suchen, der innerhalb eines Umkreises
von maximal 50 m angelegt wird. Der Verlegungsgrund ist zu dokumentieren, die sich ergebene „Ist-Koordinate“ mittels GNSS einzumessen und in der Aufnahmesoftware zu erfassen.

Ist der vorgegebene Punkt, im Sinne des LWaldG, nicht im Wald und nicht nach obigen
Parametern sinnvoll in eine Waldfläche verlegbar, ist der Punkt als „Nichtwald“ mit dem
vorhandenen Landschaftselement in der Aufnahmesoftware zu erfassen.

Liegt der Aufnahmepunkt in unmittelbarer Nähe zu oder in einem besonders geschützten Biotop, ist dieses in der Aufnahmesoftware zu erfassen.

#### Wiederholungsaufnahme

In Wiederholungserhebungen ist der Stichprobenpunkt mittels GNSS aufzusuchen und anhand des
Baumplots zu verifizieren. Die Ausführungen zu möglichen Stichprobenpunktverlegungen und zu den
nachfolgenden Aufnahmen behalten bei Wiederholungsaufnahmen ihre Gültigkeit. Sämtliche in
vorherigen Inventuren erfasste Daten sind zu kontrollieren.

### Datenerhebung für die Baumplots und die Schäl- und Fegeaufnahme

#### Baumplot 1 am Stichprobenpunkt

Vom Stichprobenpunkt (siehe Abbildung.1), sind im Umkreis von maximal 12 m von den fünf nächst
stehenden Bäumen

- die Baumart,
- der Azimut in Gon,
- die Entfernung in cm,
- der BHD in mm und
- gegebenenfalls die Messhöhe des BHD sowie
- das Vorhandensein von Schäle oder Fege der letzten 12 Monate aufzunehmen.

Zur Aufnahme des BHD ist ein Umfangsmaßband zu verwenden. Die
Aufnahme der Baumdaten erfolgt im Uhrzeigersinn.

Sind geeignete Landmarken als Orientierungspunkte vorhanden, sollen diese mit Art, Abstand
und Azimut mit aufgenommen werden. Sind im Umkreis von 12 m um den Stichprobenpunkt keine
Bäume mit einem BHD > 7 cm vorhanden, sind geeigneten Orientierungspunkten/-hilfen als Landmarken zu erfassen.
Geeignete Orientierungspunkte sind: weiter weg stehende Bäume, Stubben, Masten, usw.

#### Baumplot 2 am Ende des Transektes

Am Ende des anzulegenden Transektes, ist eine zweite Fünfbaumstichprobe (BP 2) anzulegen und
aufzunehmen. Die Beurteilung der fünf Bäume erfolgt analog des Baumplots 1. Somit wird eine
nachvollziehbare Linie vom SP über AP bis zum Ende Transekts erzeugt Die primäre Richtung ist dabei
Norden (0 Gon). Wenn in der primären Richtung einer der in [Verlegung des Stichprobenpunktes - Aufnahmepunktes](Verlegung des Stichprobenpunktes - Aufnahmepunktes) genannten Faktoren vorliegt und
kein Verjüngungstransekt (VT) angelegt werden kann, darf in eine andere Richtung ausgewichen
werden. Der Azimut vom SP in Richtung des AP und BP 2 ist zu ermitteln und in die Aufnahmesoftware zu übernehmen.

![Skizze_SP_BP1_AP_BP2.png](pics/Transekt.png)

### Verjüngungszustandserfassung im Transekt

#### Anlage – Transekt

Der Aufnahmepunkt ist in der Regel in Richtung Norden (0 Gon) zwei Meter vom Stichprobenpunkt
(SP) temporär mittels Fluchtstange zu markieren. Mithilfe einer Bussole und Laser- bzw.
Ultraschallentfernungsmesser oder Maßband ist eine Strecke von 20 m Länge zu ermitteln. Das Ende wird mit einer weiteren Fluchtstange markiert und dort der Baumplot 2 erstellt. Damit ist Baumplot 2 (BP2) 22 m vom Stichprobenpunkt (SP) entfernt.

Diese Strecke, von AP zu BP 2, bildet mit einer Breite von 2 m den Verjüngungstransekt (VT).

Wenn in der primären Richtung einer der in  [Verlegung des Stichprobenpunktes - Aufnahmepunktes](Verlegung des Stichprobenpunktes - Aufnahmepunktes) genannten Faktoren vorliegt und kein
Verjüngungstransekt (VT) angelegt werden kann, darf in eine andere Himmelsrichtung ausgewichen
werden. Der sich ergebene Azimut vom SP über den AP zum BP 2 ist in der Aufnahmesoftware zu
erfassen.

Eine temporäre Störung im Transekt ist kein Verlegungsgrund. Liegt eine temporäre Störung vor, zb Kronenreste im Transekt, wird diese in der Aufnahmesoftware vermerkt und so gut es geht die Verjüngungsaufnahme durchgeführt.

![Transekt_Stichprobenpunkt_Aufnahmepunkt](pics/Transekt_Stichprobenpunkt_Aufnahmepunkt.png)

#### Verjüngungszustandserfassung und Pflanzenanzahl

Im Verjüngungstransekt (VT) sind alle Bäume mit Baumart, Baumhöhenstufe, bei Bäumen über
2 m Höhe auch der BHD in mm und Zustand zu erheben. Die Aufnahme der Pflanzen und des
Zustandes dieser Pflanzen beginnt immer 2 Meter vom Stichprobenpunkt - am AP.

Es sind alle Verjüngungspflanzen (Waldbäume) bis zu einer Höhe von 7 Metern bzw. einem BHD
von bis 7 cm aufzunehmen. Bei Erreichen der maximalen Pflanzenanzahl von 50 Pflanzen kann die
Aufnahme beendet werden. Die Entfernung zum AP ist dann zwingend zu messen und in **dm**
gemessen in die Aufnahmesoftware einzutragen.

Die Baumhöhen sind in folgenden Stufen zu erheben:

- 1 &rarr; ab 10 cm bis 25 cm
- 2 &rarr; 25 cm bis 50 cm
- 3 &rarr; 50 cm bis 100 cm
- 4 &rarr; 100 cm bis 150 cm
- 5 &rarr; 150 cm bis 200 cm

Ab Höhe > 200 cm ist der BHD in mm mit einem Umfangsmaßband bis zu einem BHD von 7 cm zu
erfassen.

Die Zustandserfassung bezieht sich auf einen Triebverlust im oberen Drittel der Pflanze in den letzten 12 Monaten durch:

- Verbiss,
- Trockenheit,
- Frost,
- Insekten sowie
- Schäle und Fege.

Es können pro Pflanze auch mehrere Zustände erfasst werden. Zustände, deren Ursache sich hier nicht zuordnen lässt oder älter ist, werden nicht erfasst.

#### Vegetation - und Strauchschicht im Transekt

Für die Kraut- und Strauchschicht als Ganzes ist der Bodenbedeckungsanteil im Transekt zu erfassen. Wenn das Transekt verkürzt ist, wird der Anteil trotzdem auf die gesamt mögliche Transektlänge bezogen. 

Der Bodenbedeckungsanteil mit Moosen im Transekt ist seperat von der Kraut- und Strauchschicht zu erfassen.

Weiterhin ist der Bodenbedeckungsanteil bestimmter Weiserplanzen im Transekt zu erfassen.
Es kann möglich sein, dass ein Anteil für die Kraut- und Strauchschicht angegeben wird, aber kein oder ein geringerer Anteil für die Weiserpflanzen.

Zu erfassende Weiserpflanzen

Kraut:

- Brennessel
  - [Brennessel-Wikipedia](https://de.wikipedia.org/wiki/Brennnesseln)
  - [Brennnessel-naturadb](https://www.naturadb.de/pflanzen/urtica-dioica/)  
- Goldnessel
  - [Goldnessel-Wikipedia](https://de.wikipedia.org/wiki/Gew%C3%B6hnliche_Goldnessel)
  - [Goldnessel-naturadb](https://www.naturadb.de/pflanzen/lamium-galeobdolon/)
- Heidekraut - Besenheide (Calluna vulgaris) (evtl. auch Erica spec.)
  - [Heidekraut-Wikipedia](https://de.wikipedia.org/wiki/Besenheide)
  - [Heidekraut-naturadb](https://www.naturadb.de/pflanzen/gattung/heidekraeuter/)  
- Kleinblütiges Springkraut
  - [Kleinblütiges Springkraut-Wikipedia](https://de.wikipedia.org/wiki/Kleines_Springkraut)
  - [Kleinblütiges Springkraut-naturadb](https://www.naturadb.de/pflanzen/impatiens-parviflora/)  
- Maiglöckchen
  - [Maiglöcken-Wikipedia](https://de.wikipedia.org/wiki/Maigl%C3%B6ckchen)
  - [Maiglöcken-naturadb](https://www.naturadb.de/pflanzen/convallaria-majalis/)  
- Schmalblättriges Weidenröschen
  - [Schmalblättriges Weidenröschen-Wikipedia](https://de.wikipedia.org/wiki/Schmalbl%C3%A4ttriges_Weidenr%C3%B6schen)
  - [Schmalblättriges Weidenröschen-naturadb](https://www.naturadb.de/pflanzen/epilobium-angustifolium/)  
- Waldmeister
  - [Waldmeister-Wikipedia](https://de.wikipedia.org/wiki/Waldmeister)
  - [Waldmeister-naturadb](https://www.naturadb.de/pflanzen/galium-odoratum/)  
- Waldsauerklee
  - [Waldsauerklee-Wikipedia](https://de.wikipedia.org/wiki/Wald-Sauerklee)
  - [Waldsauerklee-naturadb](https://www.naturadb.de/pflanzen/oxalis-acetosella/)  
- Wegerich - Plantago spec.
  - [Wegerich-Wikipedia](https://de.wikipedia.org/wiki/Wegeriche)
  - [Wegerich-naturadb](https://www.naturadb.de/pflanzen/gattung/wegeriche/)  

Gras:  

- Drahtschmiele
  - [Drahtschmiele-Wikipedia](https://de.wikipedia.org/wiki/Draht-Schmiele)
  - [Drahtschmiele-naturadb](https://www.naturadb.de/pflanzen/deschampsia-flexuosa/)  
- Flatterbinse
  - [Flatterbinse-Wikipedia](https://de.wikipedia.org/wiki/Flatter-Binse)
  - [Flatterbinse-naturadb](https://www.naturadb.de/pflanzen/juncus-effusus/)
- Hainrispengras
  - [Hainrispengras-Wikipedia](https://de.wikipedia.org/wiki/Hain-Rispengras)
  - [Hainrispengras-naturadb](https://www.naturadb.de/pflanzen/poa-nemoralis/)
- Perlgras - Melica spec.
  - [Perlgras-Wikipedia](https://de.wikipedia.org/wiki/Perlgr%C3%A4ser)
  - [Perlgras](https://www.naturadb.de/pflanzen/gattung/perlgraeser/)
- Pfeifengras - Molinia spec.
  - [Pfeifengras-Wikipedia](https://de.wikipedia.org/wiki/Pfeifengr%C3%A4ser)
  - [Pfeifengras-naturadb](https://www.naturadb.de/pflanzen/gattung/pfeifengraeser/)
- Sandrohr/Landreitgras - Calamagrostis epigejos
  - [Sandrohr-Wikipedia](https://de.wikipedia.org/wiki/Land-Reitgras)
  - [Sandrohr-naturadb](https://www.naturadb.de/pflanzen/calamagrostis-epigejos/)
- Waldzwenke
  - [Waldzwenke-Wikipedia](https://de.wikipedia.org/wiki/Wald-Zwenke)
  - [Waldzwenke](https://www.naturadb.de/pflanzen/brachypodium-sylvaticum/)
- Winkelsegge
  - [Winkelsegge-Wikipedia](https://de.wikipedia.org/wiki/Winkel-Segge)
  - [Winkelsegge-naturadb](https://www.naturadb.de/pflanzen/carex-remota/)

Farne:

- Adlerfarn
  - [Adlerfarn-Wikipedia](https://de.wikipedia.org/wiki/Adlerfarn)
  - [Adlerfarn-naturadb](https://www.naturadb.de/pflanzen/pteridium-aquilinum/)  

Doldengewächse:

- Giersch
  - [Giersch-Wikipedia](https://de.wikipedia.org/wiki/Giersch)
  - [Giersch-naturadb](https://www.naturadb.de/pflanzen/aegopodium-podagraria/)

Beerensträucher:

- Heidelbeere
  - [Heidelbeere-Wikipedia](https://de.wikipedia.org/wiki/Heidelbeere)
  - [Heidelbeere-naturadb](https://www.naturadb.de/pflanzen/vaccinium-myrtillus/)
- Preiselbeere
  - [Preiselbeere-Wikipedia](https://de.wikipedia.org/wiki/Preiselbeere)
  - [Preiselbeere-naturadb](https://www.naturadb.de/pflanzen/vaccinium-vitis-idaea/)

Großsträucher:

- Himbeere
  - [Himbeere-Wikipedia](https://de.wikipedia.org/wiki/Himbeere)
  - [Himbeere-naturadb](https://www.naturadb.de/pflanzen/rubus-idaeus/)
- Brombeere
  - [Brombeere-Wikipedia](https://de.wikipedia.org/wiki/Brombeeren)
  - [Brombeere-naturadb](https://www.naturadb.de/pflanzen/rubus-fruticosus/)

### Bestandesbeschreibung

Vom SP ausgehend wird für einen Halbkreis in Richtung des Transektes eine Bestandesbeschreibung
durchgeführt. Dabei wird die Betriebsart, der Kronenschlussgrad, der Deckungsanteil des Unterstandes und
der Kraut- und Strauchschicht sowie der Heterogenitätsgrad erfasst.

Es ist außerdem zu erfassen, ob im Bestand ein geschütztes Biotop liegt.

Ungeachtet möglicher Bestandesabgrenzungen sind die vorkommenden Bäume mit Baumart,
Schichtzugehörigkeit und Schichtanteil, natürlicher Altersklasse, Verteilung und Entstehung als Zeile in die
Bestandesbeschreibung aufzunehmen.

![Bestandesbeschreibungshalbkreis](pics/Bestbes.png)
Bestandesbeschreibung erfolgt vom SP aus im Halbkreis um das Transekt

#### Struktur (Heterogenitätsgrad)

Die Struktur wird über den Heterogenitätsgrad auf einer Skala von 0 = sehr homogen bis 10 =
sehr heterogen beschrieben. Es ist eine gutachterliche Einschätzung zur momentanen vertikalen und
horizontalen Struktur, der Durchmesserverteilung sowie des Artenreichtums vorzunehmen.

Anzusprechen ist dabei

- die horizontale Struktur: hohe Heterogenität, wenn Veränderungen am Waldboden wie Licht-
und Schatteninseln, Wechsel von dicht und weit auseinander stehenden Bäumen, zu beobachten
sind
- vertikale Struktur: hohe Heterogenität, wenn viele verschiedene Schichten erkennbar sind
- Strukturqualität: hohe Heterogenität, wenn verschiedene Kräuter und Sträucher sowie viele
Bäume und Baumarten in unterschiedlichen natürlichen Altersklassen mit breiter
Durchmesserverteilung vorhanden sind.

#### Betriebsart

- Hochwald - HW: Der Wald ist größtenteils aus Kernwüchsen entstanden. - Standard in
Brandenburg
- Mittelwald - MW: Der Wald besteht aus Unterholz aus Stockausschlägen und Oberholz aus
Kernwüchsen.
- Niederwald - NW: Wald der sich ausschließlich durch Wurzelbrut und Stockausschlag verjüngt
und in einer Umtriebszeit von 5 - 30 Jahren bewirtschaftet wird.
- Plenterwald - PL: Sonderform des Hochwaldes mit stufiger Struktur in den Bäumen aller Alters-
und Höhenstufen auf kleinster Fläche nebeneinanderstehen und die Nutzung ausschließlich
Einzelstammweise erfolgt.

#### Kronenschlussgrad

Die Angabe des Kronenschlussgrades dient dazu die Lichtdurchlässigkeit bis auf den Boden zu
klassifizieren. Für die Ansprache sind also alle Schichten über der Unterstandsschicht einzubeziehen.
Der Kronenschlussgrad ist hier eine gutachterliche Einschätzung zur Lichtdurchlässigkeit und
entspricht im Regelfall in etwa dem Bestockungsgrad in Klammern.

- gedrängt: Die Kronen greifen tief ineinander (ca. B° 1,1)
- geschlossen: Die Kronen berühren sich mit den Zweigspitzen, der Kronenschluss ist erreicht
(ca. B° 1,0)
- locker: Der Kronenabstand ist kleiner als eine Kronenbreite (ca. B° 0,8)
- lückig: Bestand ist weitgehend geschlossen, an wenigen Stellen Unterbrechungen von mindestens einer Kronenbreite (ca. B° 0,7)
- licht: Der Kronenabstand entspricht einer Kronenbreite (ca. B° 0,6)
- räumig/ räumdig: Der Kronenabstand überschreitet eine Kronenbreite (ca. B° < 0,4)

#### Schutzmaßnahmen

Schutzmaßnahmen, die sich nicht auf Einzelpflanzen im Verjüngungstransekt beziehen aber im
Halbkreis der Bestandesbeschreibung zu finden sind, werden erfasst. Dabei geht es nur um wirksame
Schutzmaßnahmen. Wenn zu erkennen ist, dass die Fläche gezäunt ist, dieser aber wilddurchlässig ist, ist das nicht zu erfassen.

Achtung: Stichprobenpunkte, die sich in einer forstlich, gezäunten Fläche
(z.B. Kultur) befinden, sind grundsätzlich aufzunehmen. Das Betreten dieser Fläche erfolgt nur an
sicheren Ein- , bzw. Überstiegen. Jegliche Eigengefährdung, eine Gefährdung des Schutzgrundes und
eine Beschädigung der Schutzanlage ist zu vermeiden. Dabei entstehende Beschädigungen und
nachlaufende Entschädigungsforderungen gehen zu Lasten des Verursachers, in diesem Fall des
Aufnahmetrupps und der Auftragnehmer.

#### Anzahl der Schichten (n-Schichtigkeit)

Die Anzahl der Schichten ist zu erfassen.

- **einschichtig (es)**: nur Schichtart Oberstand ist vertreten
- **zweischichtig (zs)**: zur Schichtart Oberstand tritt eine weitere Schichtart hinzu
- **Ober-, Mittel- und Unterholz (3)**: Behandlungseinheit besteht aus den Schichtarten Ober-, Mittel- und Unterholz
- **mehrschichtig oder plenterartig (pl)**: Behandlungseinheit besteht aus der Schichtart Ungleichaltrig
- **mehrschichtig mit Überhälter/Nachhiebsrest (ms)**: zur Schichtart Oberstand treten zwei oder mehr weitere Schichtarten von Überhalt, Zwischenstand und / oder Unterstand hinzu

pl ist ähnlich zu ms, in ms können die Schichten jedoch noch deutlich voneinandern unterschieden werden.  

Weitere Schichten, die für die Landesinventur eine untergeordnete Rollen spielen: 
- **zweischichtig mit Vorausverjüngung**: unter Oberstand eine Verjüngung, die (noch) nicht übernahmewürdig und/oder wirtschaftlich (noch) nicht erwünscht ist
- **zweischichtig mit Unterbau**: unter Oberstand künstlicher Anbau von Baumarten unter Oberstand mit dienender Funktion zur Aufwertung des Bodenschutzes, der Baumartenvielfalt, der Schaftpflege

#### Bedeckungsanteile 

Weiterhin ist der Bodenbeckungsanteil der Kraut- und Strauchschicht und der Bedeckungsanteil der Unterstandes (falls vorhanden) im Bestand in der Eingabemaske zu erfassen.

### Schichtweise Baumerfassung (Bestockung)

Die im Halbkreis vorkommenden Bäume sind mit ihrer Schichtzugehörigkeit, der Baumart, der
natürlichen Altersklasse, der Verteilung, der Entstehungsart und ihrem Schichtanteil zu erfassen. Der Anteil
der zu erfassenden Bäume muss mindestens 1 % betragen. Der Anteil aller Baumarten einer Schicht
darf 100 % nicht übersteigen.

Die Schichtzuordnung ist folgendermaßen vorzunehmen:

- **Plenterwald**: keine Zuordnung möglich. Eindeutige ungleichaltrige Struktur des Gesamtbestandes
- **Oberstand (OS)** oberes Bestandesdrittel
- **Mittelholz (MH)**: mittleres Bestandesdrittel, nur Abgrenzen wenn Kronenbereiche sich
wirklich nicht überlagern und es eine Derbholzgrenze aufweist
- **Unterschicht**: unteres Bestandesdrittel
- **Überhalt**: Rest des Vorbestandes im Altersklassenwald (nur wenn eindeutig), mit annährnd gleichmäßiger Verteilung auf der Fläche
- **Liegend**: liegende, lebende Bestandesschicht, z.B. nach Windwurf (selten); Terminaltrieb befindet sich in einer anderen Schicht, als aufgrund der Länge der Stammachse zu erwarten wäre
- **Altbäume - Restvorrat** -  	alte Einzelbäume, die nicht der Schichtart UeH zugeorndet werden können; nur anzuwenden wenn eindeutig „Restvorräte“ vorhanden sind, in der Regel
sind das Habitatbäume, Bestände mit Altbestand haben eindeutig eine höhere Biotopstruktur
- **Hauptbestand**: das wirtschaftliche Schwergewicht - einschätzbar nur nach Rücksprache mit Bewirtschafter
- **Verjüngung unter Schirm** Verjüngung wird sich aufgrund ihrer Dichte, Qualität und Baumartenzusammensetzung zum Fogebestand entwickeln; nur anzuwenden wenn deutlich ist, dass „Vorausverjüngung“
vorhanden aber wirtschaftlich noch nicht relevant ist und eventuell nicht übernommen wird


#### Bodenvegetation

Für einige Vegetationsgruppen der Moos-, Kraut- und Strauchschicht ist der Bodenbededeckungsanteil im Bestad anzugeben.
Die Summe der Anteile darf abzüglich des Mooses 100 % nicht übersteigen.

### Störungen im Bestand

Wenn auf der Fläche des Halbkreises aktuelle, **verjüngungsbeeinflussende** Störungen zu erkennen
sind, sollen diese erfasst werden. Als Störungen gelten:

- Durchforstung
- Sanitärhiebe
- Waldbrand
- Sturm
- Bodenbearbeitung

## Anhang

### PDF-Erzeugung

Aus dieser Datei kann ein PDF mit

```bash
pandoc Verfahrensanleitung_VWM.md -o <Datum>_Verfahrensanleitung_VWM.pdf
```

erzeugt werden.
